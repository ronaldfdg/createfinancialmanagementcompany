package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Reviewer;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class CreateReviewerSubscriptionRequestMapperTest {

    @InjectMocks
    private CreateReviewerSubscriptionRequestMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapBackendValues();
    }

    private void mapBackendValues() {
        Mockito.when(enumMapper.getBackendValue("suscriptionRequest.reviewer.id", EntityStubs.SUSCRIPTION_REQUEST_REVIEWER_ID_ENUM)).thenReturn(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("contactDetails.contactType.id", EntityStubs.EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_ENUM)).thenReturn(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND);
    }


    @Test
    public void mapInFullTest() throws IOException {
        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNotNull(result.getReviewer().getReviewerType().getId());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContactType());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContact());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result
                .getReviewer().getReviewerType().getId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND,
                result.getReviewer().getContactDetails().get(0)
                        .getContactType());
        assertEquals(input.getContactDetails().get(0).getContact(),
                result.getReviewer().getContactDetails().get(0).getContact());
    }

    @Test
    public void mapInWithoutReviewerTypeEmptyTest() throws IOException {

        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        result.getReviewer().setReviewerType(null);

        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNull(result.getReviewer().getReviewerType());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContactType());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContact());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND,
                result.getReviewer().getContactDetails().get(0)
                        .getContactType());
        assertEquals(input.getContactDetails().get(0).getContact(),
                result.getReviewer().getContactDetails().get(0).getContact());
    }

    @Test
    public void mapInWithoutReviewerTypeIdEmptyTest() throws IOException {

        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        result.getReviewer().getReviewerType().setId(null);

        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNull(result.getReviewer().getReviewerType().getId());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContactType());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContact());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND,
                result.getReviewer().getContactDetails().get(0)
                        .getContactType());
        assertEquals(input.getContactDetails().get(0).getContact(),
                result.getReviewer().getContactDetails().get(0).getContact());
    }


    @Test
    public void mapInWithoutContactDetailsEmptyTest() throws IOException {

        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        result.getReviewer().setContactDetails(null);

        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNotNull(result.getReviewer().getReviewerType().getId());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNull(result.getReviewer().getContactDetails());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
    }

    @Test
    public void mapInWithoutContactTypeOfListContactDetailsEmptyTest() throws IOException {
        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        result.getReviewer().getContactDetails().get(0).setContactType(null);

        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNotNull(result.getReviewer().getReviewerType().getId());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNull(result.getReviewer().getContactDetails().get(0)
                .getContactType());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContact());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result
                .getReviewer().getReviewerType().getId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
        assertEquals(input.getContactDetails().get(0).getContact(),
                result.getReviewer().getContactDetails().get(0).getContact());
    }

    @Test
    public void mapInWithoutContactOfListContactDetailsEmptyTest() throws IOException {
        Reviewer input = EntityStubs.getInstance().getReviewer();
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, input);
        result.getReviewer().getContactDetails().get(0).setContact(null);

        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer().getBusinessAgentId());
        assertNotNull(result.getReviewer().getReviewerType().getId());
        assertNotNull(result.getReviewer().getFirstName());
        assertNotNull(result.getReviewer().getMiddleName());
        assertNotNull(result.getReviewer().getLastName());
        assertNotNull(result.getReviewer().getSecondLastName());
        assertNotNull(result.getReviewer().getContactDetails().get(0)
                .getContactType());
        assertNull(result.getReviewer().getContactDetails().get(0)
                .getContact());
        assertEquals(SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        assertEquals(input.getBusinessAgentId(), result.getReviewer()
                .getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result
                .getReviewer().getReviewerType().getId());
        assertEquals(input.getFirstName(), result.getReviewer()
                .getFirstName());
        assertEquals(input.getMiddleName(), result.getReviewer()
                .getMiddleName());
        assertEquals(input.getLastName(), result.getReviewer()
                .getLastName());
        assertEquals(input.getSecondLastName(), result.getReviewer()
                .getSecondLastName());
        assertEquals(EMAIL_CONTACTS_DETAILS_CONTACT_TYPE_ID_BACKEND,
                result.getReviewer().getContactDetails().get(0)
                        .getContactType());
    }

    @Test
    public void mapInEmptyTest() {
        InputCreateReviewerSubscriptionRequest result = mapper.mapIn(
                SUBSCRIPTION_REQUEST_ID, new Reviewer());
        assertNotNull(result);
        assertNotNull(result.getSubscriptionRequestId());
        assertNotNull(result.getReviewer());
        assertNull(result.getReviewer().getBusinessAgentId());
        assertNull(result.getReviewer().getReviewerType());
        assertNull(result.getReviewer().getFirstName());
        assertNull(result.getReviewer().getMiddleName());
        assertNull(result.getReviewer().getLastName());
        assertNull(result.getReviewer().getSecondLastName());
        assertNull(result.getReviewer().getContactDetails());
    }
}
