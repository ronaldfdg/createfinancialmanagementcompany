package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBEHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBSHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.mock.FormatsKwhfMock;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;

public class TxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapperTest {

    @InjectMocks
    private TxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper mapper;

    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        mapper = new TxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper();
        MockitoAnnotations.initMocks(this);
        mockValue();
    }

    private void mockValue() {
        Mockito.when(enumMapper.getEnumValue("businessManagers.contactType.id", EntityStubs.ENUM_BACKEND_WORK_EMAIL)).thenReturn(EntityStubs.WORK_EMAIL);
    }

    @Test
    public void testMapIn() {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager input = new InputSendEmailOtpFinancialManagementCompaniesBusinessManager();
        input.setBusinessManagerId(EntityStubs.BUSINESS_MANAGERID);

        FormatoKNECBEHF result = mapper.mapIn(input);

        assertNotNull(result);
        assertEquals(input.getBusinessManagerId(), result.getUsuid());
    }

    @Test
    public void mapOutTest() throws IOException {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager input = new InputSendEmailOtpFinancialManagementCompaniesBusinessManager();
        input.setBusinessManagerId(EntityStubs.BUSINESS_MANAGERID);

        FormatoKNECBSHF formato = FormatsKwhfMock.getInstance().getFormatoKNECBSHF().get(0);
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager result = mapper.mapOut(formato);

        assertNotNull(result);

        assertNotNull(result.getEmailList());
        assertNotNull(result.getBusinessName());
        assertNotNull(result.getUserName());

        assertEquals(formato.getNomemp(), result.getBusinessName());
        assertEquals(formato.getNomusu(), result.getUserName());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(0).getType().getId());
        assertEquals(formato.getDaco0(), result.getEmailList().get(0).getValue());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(1).getType().getId());
        assertEquals(formato.getDaco1(), result.getEmailList().get(1).getValue());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(2).getType().getId());
        assertEquals(formato.getDaco2(), result.getEmailList().get(2).getValue());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(3).getType().getId());
        assertEquals(formato.getDaco3(), result.getEmailList().get(3).getValue());

        formato = FormatsKwhfMock.getInstance().getFormatoKNECBSHF().get(1);
        result = mapper.mapOut(formato);

        assertNotNull(result);

        assertNotNull(result.getEmailList());
        assertNotNull(result.getBusinessName());
        assertNotNull(result.getUserName());

        assertEquals(formato.getNomemp(), result.getBusinessName());
        assertEquals(formato.getNomusu(), result.getUserName());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(0).getType().getId());
        assertEquals(formato.getDaco0(), result.getEmailList().get(0).getValue());
        assertEquals(EntityStubs.WORK_EMAIL, result.getEmailList().get(1).getType().getId());
        assertEquals(formato.getDaco1(), result.getEmailList().get(1).getValue());

        formato = FormatsKwhfMock.getInstance().getFormatoKNECBSHF().get(2);
        result = mapper.mapOut(formato);

        assertNotNull(result);

        assertNull(result.getEmailList());
        assertNull(result.getBusinessName());
        assertNull(result.getUserName());
    }
}
