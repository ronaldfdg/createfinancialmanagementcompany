package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany.ModelFinancialManagementCompaniesRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany.ModelFinancialManagementCompaniesResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class RestCreateFinancialManagementCompanyMapperTest {

  private RestCreateFinancialManagementCompanyMapper mapper;

  @Before
  public void setup() {
    mapper = new RestCreateFinancialManagementCompanyMapper();
  }

  @Test
  public void mapInFullTest() throws IOException {
    DTOIntFinancialManagementCompanies input = EntityStubs.getInstance().getDTOIntFinancialManagementCompanies();
    ModelFinancialManagementCompaniesRequest result = mapper.mapIn(input);

    assertNotNull(result);
    assertNotNull(result.getBusiness());
    assertNotNull(result.getBusiness().getBusinessDocuments());
    assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType());
    assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
    assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
    assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
    assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
    assertNotNull(result.getBusiness().getLimitAmount());
    assertNotNull(result.getBusiness().getLimitAmount().getAmount());
    assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
    assertNotNull(result.getBusiness().getBusinessManagement());
    assertNotNull(result.getBusiness().getBusinessManagement().getManagementType());
    assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
    assertNotNull(result.getNetcashType());
    assertNotNull(result.getNetcashType().getId());
    assertNotNull(result.getNetcashType().getVersion());
    assertNotNull(result.getNetcashType().getVersion().getId());
    assertNotNull(result.getContract());
    assertNotNull(result.getContract().getId());
    assertNotNull(result.getRelatedProduct());
    assertNotNull(result.getRelatedProduct().getId());
    assertNotNull(result.getRelatedProduct().getProductType());
    assertNotNull(result.getRelatedProduct().getProductType().getId());
    assertNotNull(result.getRelationType());
    assertNotNull(result.getRelationType().getId());
    assertNotNull(result.getReviewers());
    assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
    assertNotNull(result.getReviewers().get(0).getContactDetails());
    assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
    assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
    assertNotNull(result.getReviewers().get(0).getReviewerType());
    assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
    assertNotNull(result.getReviewers().get(0).getUnitManagement());
    assertNotNull(result.getReviewers().get(0).getBank());
    assertNotNull(result.getReviewers().get(0).getBank().getId());
    assertNotNull(result.getReviewers().get(0).getBank().getBranch());
    assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
    assertNotNull(result.getReviewers().get(0).getProfile());
    assertNotNull(result.getReviewers().get(0).getProfile().getId());
    assertNotNull(result.getReviewers().get(0).getProfessionPosition());
    assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());

    assertEquals(input.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId(),
                  result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
    assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(),
                  result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
    assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(),
                  result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
    assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(),
                  result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
    assertEquals(input.getBusiness().getBusinessManagement().getManagementType().getId(),
                  result.getBusiness().getBusinessManagement().getManagementType().getId());
    assertEquals(input.getBusiness().getLimitAmount().getAmount(),
                  result.getBusiness().getLimitAmount().getAmount());
    assertEquals(input.getBusiness().getLimitAmount().getCurrency(),
                  result.getBusiness().getLimitAmount().getCurrency());
    assertEquals(input.getNetcashType().getId(),
                  result.getNetcashType().getId());
    assertEquals(input.getNetcashType().getVersion().getId(),
                  result.getNetcashType().getVersion().getId());
    assertEquals(input.getContract().getId(),
                  result.getContract().getId());
    assertEquals(input.getProduct().getId(),
                  result.getRelatedProduct().getId());
    assertEquals(input.getProduct().getProductType().getId(),
                  result.getRelatedProduct().getProductType().getId());
    assertEquals(input.getRelationType().getId(),
                  result.getRelationType().getId());
    assertEquals(input.getReviewers().get(0).getBusinessAgentId(),
                  result.getReviewers().get(0).getBusinessAgentId());
    assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(),
                  result.getReviewers().get(0).getContactDetails().get(0).getContact());
    assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContactType(),
                  result.getReviewers().get(0).getContactDetails().get(0).getContactType());
    assertEquals(input.getReviewers().get(0).getReviewerType().getId(),
                  result.getReviewers().get(0).getReviewerType().getId());
    assertEquals(input.getReviewers().get(0).getUnitManagement(),
                  result.getReviewers().get(0).getUnitManagement());
    assertEquals(input.getReviewers().get(0).getBank().getId(),
                  result.getReviewers().get(0).getBank().getId());
    assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(),
                  result.getReviewers().get(0).getBank().getBranch().getId());
    assertEquals(input.getReviewers().get(0).getProfile().getId(),
                  result.getReviewers().get(0).getProfile().getId());
    assertEquals(input.getReviewers().get(0).getProfessionPosition(),
                  result.getReviewers().get(0).getProfessionPosition());
    assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(),
                  result.getReviewers().get(0).getRegistrationIdentifier());

  }

  @Test
  public void mapInEmptyTest() {
    ModelFinancialManagementCompaniesRequest result = mapper.mapIn(new DTOIntFinancialManagementCompanies());
    assertNotNull(result);
    assertNull(result.getBusiness());
    assertNull(result.getNetcashType());
    assertNull(result.getContract());
    assertNull(result.getRelatedProduct());
    assertNull(result.getRelationType());
    assertNull(result.getReviewers());
  }

  @Test
  public void mapOutFullTest() throws IOException {
    ModelFinancialManagementCompaniesResponse response = EntityStubs.getInstance().getModelFinancialManagementCompaniesResponse();
    FinancialManagementCompanies result = mapper.mapOut(response);
    assertNotNull(result);
    assertNotNull(result.getId());
  }

  @Test
  public void mapOutEmptyTest() {
    FinancialManagementCompanies result = mapper.mapOut(null);
    assertNull(result);
  }

}
