package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.FormatoKNECBEHB;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesRelatedContractsMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
public class TxCreateFinancialManagementCompaniesRelatedContractsMapperTest {

    private ITxCreateFinancialManagementCompaniesRelatedContractsMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxCreateFinancialManagementCompaniesRelatedContractsMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputCreateFinancialManagementCompaniesRelatedContracts input = EntityStubs.getInstance().getInputCreateFinancialManagementCompaniesRelatedContracts();

        FormatoKNECBEHB result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCodemp());
        assertNotNull(result.getCta01());
        assertNotNull(result.getIdepr1());
        assertNotNull(result.getIdtpr1());
        assertNotNull(result.getIdrpr1());
        assertNotNull(result.getCta02());
        assertNotNull(result.getIdepr2());
        assertNotNull(result.getIdtpr2());
        assertNotNull(result.getIdrpr2());
        assertNotNull(result.getCta03());
        assertNotNull(result.getIdepr3());
        assertNotNull(result.getIdtpr3());
        assertNotNull(result.getIdrpr3());
        assertNotNull(result.getCta04());
        assertNotNull(result.getIdepr4());
        assertNotNull(result.getIdtpr4());
        assertNotNull(result.getIdrpr4());
        assertNotNull(result.getCta05());
        assertNotNull(result.getIdepr5());
        assertNotNull(result.getIdtpr5());
        assertNotNull(result.getIdrpr5());
        assertNotNull(result.getCta06());
        assertNotNull(result.getIdepr6());
        assertNotNull(result.getIdtpr6());
        assertNotNull(result.getIdrpr6());
        assertNotNull(result.getCta07());
        assertNotNull(result.getIdepr7());
        assertNotNull(result.getIdtpr7());
        assertNotNull(result.getIdrpr7());
        assertNotNull(result.getCta08());
        assertNotNull(result.getIdepr8());
        assertNotNull(result.getIdtpr8());
        assertNotNull(result.getIdrpr8());
        assertNotNull(result.getCta09());
        assertNotNull(result.getIdepr9());
        assertNotNull(result.getIdtpr9());
        assertNotNull(result.getIdrpr9());
        assertNotNull(result.getCta10());
        assertNotNull(result.getIdepr10());
        assertNotNull(result.getIdtpr10());
        assertNotNull(result.getIdrpr10());
        assertNotNull(result.getCta11());
        assertNotNull(result.getIdepr11());
        assertNotNull(result.getIdtpr11());
        assertNotNull(result.getIdrpr11());
        assertNotNull(result.getCta12());
        assertNotNull(result.getIdepr12());
        assertNotNull(result.getIdtpr12());
        assertNotNull(result.getIdrpr12());
        assertNotNull(result.getCta13());
        assertNotNull(result.getIdepr13());
        assertNotNull(result.getIdtpr13());
        assertNotNull(result.getIdrpr13());
        assertNotNull(result.getCta14());
        assertNotNull(result.getIdepr14());
        assertNotNull(result.getIdtpr14());
        assertNotNull(result.getIdrpr14());
        assertNotNull(result.getCta15());
        assertNotNull(result.getIdepr15());
        assertNotNull(result.getIdtpr15());
        assertNotNull(result.getIdrpr15());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCodemp());
        assertEquals(input.getRelatedContracts().get(0).getContract().getId(), result.getCta01());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getId(), result.getIdepr1());
        assertEquals(input.getRelatedContracts().get(0).getProduct().getProductType().getId(), result.getIdtpr1());
        assertEquals(input.getRelatedContracts().get(0).getRelationType().getId(), result.getIdrpr1());
        assertEquals(input.getRelatedContracts().get(1).getContract().getId(), result.getCta02());
        assertEquals(input.getRelatedContracts().get(1).getProduct().getId(), result.getIdepr2());
        assertEquals(input.getRelatedContracts().get(1).getProduct().getProductType().getId(), result.getIdtpr2());
        assertEquals(input.getRelatedContracts().get(1).getRelationType().getId(), result.getIdrpr2());
        assertEquals(input.getRelatedContracts().get(2).getContract().getId(), result.getCta03());
        assertEquals(input.getRelatedContracts().get(2).getProduct().getId(), result.getIdepr3());
        assertEquals(input.getRelatedContracts().get(2).getProduct().getProductType().getId(), result.getIdtpr3());
        assertEquals(input.getRelatedContracts().get(2).getRelationType().getId(), result.getIdrpr3());
        assertEquals(input.getRelatedContracts().get(3).getContract().getId(), result.getCta04());
        assertEquals(input.getRelatedContracts().get(3).getProduct().getId(), result.getIdepr4());
        assertEquals(input.getRelatedContracts().get(3).getProduct().getProductType().getId(), result.getIdtpr4());
        assertEquals(input.getRelatedContracts().get(3).getRelationType().getId(), result.getIdrpr4());
        assertEquals(input.getRelatedContracts().get(4).getContract().getId(), result.getCta05());
        assertEquals(input.getRelatedContracts().get(4).getProduct().getId(), result.getIdepr5());
        assertEquals(input.getRelatedContracts().get(4).getProduct().getProductType().getId(), result.getIdtpr5());
        assertEquals(input.getRelatedContracts().get(4).getRelationType().getId(), result.getIdrpr5());
        assertEquals(input.getRelatedContracts().get(5).getContract().getId(), result.getCta06());
        assertEquals(input.getRelatedContracts().get(5).getProduct().getId(), result.getIdepr6());
        assertEquals(input.getRelatedContracts().get(5).getProduct().getProductType().getId(), result.getIdtpr6());
        assertEquals(input.getRelatedContracts().get(5).getRelationType().getId(), result.getIdrpr6());
        assertEquals(input.getRelatedContracts().get(6).getContract().getId(), result.getCta07());
        assertEquals(input.getRelatedContracts().get(6).getProduct().getId(), result.getIdepr7());
        assertEquals(input.getRelatedContracts().get(6).getProduct().getProductType().getId(), result.getIdtpr7());
        assertEquals(input.getRelatedContracts().get(6).getRelationType().getId(), result.getIdrpr7());
        assertEquals(input.getRelatedContracts().get(7).getContract().getId(), result.getCta08());
        assertEquals(input.getRelatedContracts().get(7).getProduct().getId(), result.getIdepr8());
        assertEquals(input.getRelatedContracts().get(7).getProduct().getProductType().getId(), result.getIdtpr8());
        assertEquals(input.getRelatedContracts().get(7).getRelationType().getId(), result.getIdrpr8());
        assertEquals(input.getRelatedContracts().get(8).getContract().getId(), result.getCta09());
        assertEquals(input.getRelatedContracts().get(8).getProduct().getId(), result.getIdepr9());
        assertEquals(input.getRelatedContracts().get(8).getProduct().getProductType().getId(), result.getIdtpr9());
        assertEquals(input.getRelatedContracts().get(8).getRelationType().getId(), result.getIdrpr9());
        assertEquals(input.getRelatedContracts().get(9).getContract().getId(), result.getCta10());
        assertEquals(input.getRelatedContracts().get(9).getProduct().getId(), result.getIdepr10());
        assertEquals(input.getRelatedContracts().get(9).getProduct().getProductType().getId(), result.getIdtpr10());
        assertEquals(input.getRelatedContracts().get(9).getRelationType().getId(), result.getIdrpr10());
        assertEquals(input.getRelatedContracts().get(10).getContract().getId(), result.getCta11());
        assertEquals(input.getRelatedContracts().get(10).getProduct().getId(), result.getIdepr11());
        assertEquals(input.getRelatedContracts().get(10).getProduct().getProductType().getId(), result.getIdtpr11());
        assertEquals(input.getRelatedContracts().get(10).getRelationType().getId(), result.getIdrpr11());
        assertEquals(input.getRelatedContracts().get(11).getContract().getId(), result.getCta12());
        assertEquals(input.getRelatedContracts().get(11).getProduct().getId(), result.getIdepr12());
        assertEquals(input.getRelatedContracts().get(11).getProduct().getProductType().getId(), result.getIdtpr12());
        assertEquals(input.getRelatedContracts().get(11).getRelationType().getId(), result.getIdrpr12());
        assertEquals(input.getRelatedContracts().get(12).getContract().getId(), result.getCta13());
        assertEquals(input.getRelatedContracts().get(12).getProduct().getId(), result.getIdepr13());
        assertEquals(input.getRelatedContracts().get(12).getProduct().getProductType().getId(), result.getIdtpr13());
        assertEquals(input.getRelatedContracts().get(12).getRelationType().getId(), result.getIdrpr13());
        assertEquals(input.getRelatedContracts().get(13).getContract().getId(), result.getCta14());
        assertEquals(input.getRelatedContracts().get(13).getProduct().getId(), result.getIdepr14());
        assertEquals(input.getRelatedContracts().get(13).getProduct().getProductType().getId(), result.getIdtpr14());
        assertEquals(input.getRelatedContracts().get(13).getRelationType().getId(), result.getIdrpr14());
        assertEquals(input.getRelatedContracts().get(14).getContract().getId(), result.getCta15());
        assertEquals(input.getRelatedContracts().get(14).getProduct().getId(), result.getIdepr15());
        assertEquals(input.getRelatedContracts().get(14).getProduct().getProductType().getId(), result.getIdtpr15());
        assertEquals(input.getRelatedContracts().get(14).getRelationType().getId(), result.getIdrpr15());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECBEHB result = mapper.mapIn(new InputCreateFinancialManagementCompaniesRelatedContracts());

        assertNotNull(result);
        assertNull(result.getCodemp());
        assertNull(result.getCta01());
        assertNull(result.getIdepr1());
        assertNull(result.getIdtpr1());
        assertNull(result.getIdrpr1());
        assertNull(result.getCta02());
        assertNull(result.getIdepr2());
        assertNull(result.getIdtpr2());
        assertNull(result.getIdrpr2());
        assertNull(result.getCta03());
        assertNull(result.getIdepr3());
        assertNull(result.getIdtpr3());
        assertNull(result.getIdrpr3());
        assertNull(result.getCta04());
        assertNull(result.getIdepr4());
        assertNull(result.getIdtpr4());
        assertNull(result.getIdrpr4());
        assertNull(result.getCta05());
        assertNull(result.getIdepr5());
        assertNull(result.getIdtpr5());
        assertNull(result.getIdrpr5());
        assertNull(result.getCta06());
        assertNull(result.getIdepr6());
        assertNull(result.getIdtpr6());
        assertNull(result.getIdrpr6());
        assertNull(result.getCta07());
        assertNull(result.getIdepr7());
        assertNull(result.getIdtpr7());
        assertNull(result.getIdrpr7());
        assertNull(result.getCta08());
        assertNull(result.getIdepr8());
        assertNull(result.getIdtpr8());
        assertNull(result.getIdrpr8());
        assertNull(result.getCta09());
        assertNull(result.getIdepr9());
        assertNull(result.getIdtpr9());
        assertNull(result.getIdrpr9());
        assertNull(result.getCta10());
        assertNull(result.getIdepr10());
        assertNull(result.getIdtpr10());
        assertNull(result.getIdrpr10());
        assertNull(result.getCta11());
        assertNull(result.getIdepr11());
        assertNull(result.getIdtpr11());
        assertNull(result.getIdrpr11());
        assertNull(result.getCta12());
        assertNull(result.getIdepr12());
        assertNull(result.getIdtpr12());
        assertNull(result.getIdrpr12());
        assertNull(result.getCta13());
        assertNull(result.getIdepr13());
        assertNull(result.getIdtpr13());
        assertNull(result.getIdrpr13());
        assertNull(result.getCta14());
        assertNull(result.getIdepr14());
        assertNull(result.getIdtpr14());
        assertNull(result.getIdrpr14());
        assertNull(result.getCta15());
        assertNull(result.getIdepr15());
        assertNull(result.getIdtpr15());
        assertNull(result.getIdrpr15());
    }
}
