package com.bbva.pzic.financialmanagementcompanies.dao.model;

/**
 * Created on 04/09/2018.
 *
 * @author Entelgy
 */
public final class BackendParamsNames {

    public static final String SUBSCRIPTION_REQUEST_ID = "subscription-request-id";
    public static final String BUSINESS_MANAGER_ID = "business-manager-id";

    private BackendParamsNames() {
    }
}
