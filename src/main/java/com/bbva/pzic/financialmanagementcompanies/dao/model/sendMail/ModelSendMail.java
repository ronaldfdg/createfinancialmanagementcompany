package com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.util.List;

public class ModelSendMail {

    @DatoAuditable
    private String id;
    @DatoAuditable
    private String subject;
    private String sender;
    @DatoAuditable
    private List<String> recipients;
    private List<String> messageBody;
    @DatoAuditable
    private String codeTemplate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public List<String> getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(List<String> messageBody) {
        this.messageBody = messageBody;
    }

    public String getCodeTemplate() {
        return codeTemplate;
    }

    public void setCodeTemplate(String codeTemplate) {
        this.codeTemplate = codeTemplate;
    }
}
