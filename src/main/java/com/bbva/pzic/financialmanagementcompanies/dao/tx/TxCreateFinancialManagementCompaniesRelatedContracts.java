package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateFinancialManagementCompaniesRelatedContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.FormatoKNECBEHB;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.PeticionTransaccionKwhb;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhb.RespuestaTransaccionKwhb;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesRelatedContractsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.NoneOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
@Component("txCreateFinancialManagementCompaniesRelatedContracts")
public class TxCreateFinancialManagementCompaniesRelatedContracts
        extends
        NoneOutputFormat<InputCreateFinancialManagementCompaniesRelatedContracts, FormatoKNECBEHB> {

    @Resource(name = "txCreateFinancialManagementCompaniesRelatedContractsMapper")
    private ITxCreateFinancialManagementCompaniesRelatedContractsMapper mapper;

    public TxCreateFinancialManagementCompaniesRelatedContracts(@Qualifier("transaccionKwhb") InvocadorTransaccion<PeticionTransaccionKwhb, RespuestaTransaccionKwhb> transaction) {
        super(transaction, PeticionTransaccionKwhb::new);
    }

    @Override
    protected FormatoKNECBEHB mapInput(InputCreateFinancialManagementCompaniesRelatedContracts inputCreateFinancialManagementCompaniesRelatedContracts) {
        return mapper.mapIn(inputCreateFinancialManagementCompaniesRelatedContracts);
    }
}