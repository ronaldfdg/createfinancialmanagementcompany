package com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

public class ModelFinancialManagementCompaniesResponse {

  private ModelFinancialManagementCompaniesData data;
  private List<Message> messages;

  public ModelFinancialManagementCompaniesData getData() {
    return data;
  }

  public void setData(ModelFinancialManagementCompaniesData data) {
    this.data = data;
  }

  public List<Message> getMessages() {
    return messages;
  }

  public void setMessages(List<Message> messages) {
    this.messages = messages;
  }

}
