package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanies;

public interface IRestCreateFinancialManagementCompanyMapper {

  ModelFinancialManagementCompanies mapIn(DTOIntFinancialManagementCompanies dtoInt);

}
