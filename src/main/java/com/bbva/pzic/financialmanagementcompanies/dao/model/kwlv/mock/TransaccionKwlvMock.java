package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.PeticionTransaccionKwlv;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.RespuestaTransaccionKwlv;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Component("transaccionKwlv")
public class TransaccionKwlvMock implements InvocadorTransaccion<PeticionTransaccionKwlv, RespuestaTransaccionKwlv> {

    public static final String TEST_EMPTY = "66666666";
    public static final String TEST_NO_RESPONSE = "77777777";

    private FormatsKwlvMock formatsKwlvMock = FormatsKwlvMock.getInstance();

    @Override
    public RespuestaTransaccionKwlv invocar(PeticionTransaccionKwlv transaccion) {
        RespuestaTransaccionKwlv response = new RespuestaTransaccionKwlv();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoKNECLVE0 format = transaccion.getCuerpo().getParte(FormatoKNECLVE0.class);

        if (TEST_NO_RESPONSE.equals(format.getCodemp())) {
            return response;
        }
        try {
            if (TEST_EMPTY.equals(format.getCodemp())) {
                response.getCuerpo().getPartes().add(buildCopySalida(formatsKwlvMock.getFormatoKNECLVS0Empty()));
            } else {
                response.getCuerpo().getPartes().add(buildCopySalida(formatsKwlvMock.getFormatoKNECLVS0()));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return response;
    }

    @Override
    public RespuestaTransaccionKwlv invocarCache(PeticionTransaccionKwlv transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }

    private CopySalida buildCopySalida(FormatoKNECLVS0 formatoKNECLVS0) {
        CopySalida copySalida = new CopySalida();
        copySalida.setCopy(formatoKNECLVS0);
        return copySalida;
    }
}
