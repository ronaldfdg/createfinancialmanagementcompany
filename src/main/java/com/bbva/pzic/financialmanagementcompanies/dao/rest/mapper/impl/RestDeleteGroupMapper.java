package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.BodyDataRest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestDeleteGroupMaper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.common.IRequestBackendMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Mapper("deleteGroupMapper")
public class RestDeleteGroupMapper implements IRestDeleteGroupMaper {

    @Autowired
    private IRequestBackendMapper requestBackendMapper;

    @Override
    public Map<String, String> mapInHeader(InputRequestBackendRest inputRequestBackendRest) {
        return requestBackendMapper.mapInHeaderDataRest(inputRequestBackendRest.getHeader());
    }

    @Override
    public BodyDataRest mapIn(InputRequestBackendRest entityPayload) {
        BodyDataRest bodyDataRest = requestBackendMapper.mapInBodyDataRest(entityPayload.getRequestBody());
        bodyDataRest.getDataOperation().setPdgroup(entityPayload.getRequestBody().getDataOperationPdgroup());
        return bodyDataRest;
    }
}
