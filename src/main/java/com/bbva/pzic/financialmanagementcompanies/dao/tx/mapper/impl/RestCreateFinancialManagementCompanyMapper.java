package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.IRestCreateFinancialManagementCompanyMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {

  private static final Log LOG = LogFactory.getLog(RestCreateFinancialManagementCompanyMapper.class);

  @Override
  public ModelFinancialManagementCompanies mapIn(DTOIntFinancialManagementCompanies dtoInt) {
    return null;
  }
}
