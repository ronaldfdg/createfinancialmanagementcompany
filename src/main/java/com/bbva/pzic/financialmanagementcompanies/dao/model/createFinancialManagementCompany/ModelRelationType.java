package com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany;

public class ModelRelationType {

  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

}
