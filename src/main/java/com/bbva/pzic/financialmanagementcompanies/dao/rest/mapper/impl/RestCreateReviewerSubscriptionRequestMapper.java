package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ContactDetail;
import com.bbva.pzic.financialmanagementcompanies.dao.model.BackendParamsNames;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelContactInformation;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelReviewerSubscriptionResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateReviewerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestCreateReviewerSubscriptionRequestMapper extends ConfigurableMapper implements IRestCreateReviewerSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(RestCreateReviewerSubscriptionRequestMapper.class);

    @Override
    public Map<String, String> mapInPathParams(final InputCreateReviewerSubscriptionRequest input) {
        LOG.info("... called method RestCreateReviewerSubscriptionRequestMapper.mapInPathParams ...");
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put(BackendParamsNames.SUBSCRIPTION_REQUEST_ID, input.getSubscriptionRequestId());
        return pathParams;
    }

    @Override
    public ModelReviewerSubscriptionRequest mapIn(
            final InputCreateReviewerSubscriptionRequest reviewerSubscriptionRequest) {
        LOG.info("... called method RestCreateReviewerSubscriptionRequestMapper.mapIn ...");
        ModelReviewerSubscriptionRequest dtoInt = map(reviewerSubscriptionRequest,
                ModelReviewerSubscriptionRequest.class);
        if (reviewerSubscriptionRequest.getReviewer() != null) {
            dtoInt.setContactInformations(mapContactInformation(reviewerSubscriptionRequest.getReviewer().getContactDetails()));
        }
        return dtoInt;
    }

    @Override
    public String mapOut(ModelReviewerSubscriptionResponse response) {
        LOG.info("... called method RestCreateReviewerSubscriptionRequestMapper.mapOut ...");
        if (response != null && response.getParticipant() != null)
            return response.getParticipant().getCode();
        else
            return null;
    }

    private List<ModelContactInformation> mapContactInformation(List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return null;
        }
        List<ModelContactInformation> dtos = new ArrayList<>();
        for (ContactDetail canonic : contactDetails) {
            ModelContactInformation modelContactInformation = new ModelContactInformation();
            modelContactInformation.setId(canonic.getContactType());
            modelContactInformation.setContact(canonic.getContact());
            dtos.add(modelContactInformation);
        }
        return dtos;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(InputCreateReviewerSubscriptionRequest.class,
                ModelReviewerSubscriptionRequest.class)
                .field("subscriptionRequestId", "form.id")
                .field("reviewer.businessAgentId", "participant.code")
                .field("reviewer.reviewerType.id", "participantType.id")
                .field("reviewer.firstName", "firstName")
                .field("reviewer.middleName", "middleName")
                .field("reviewer.lastName", "lastName")
                .field("reviewer.secondLastName", "motherLastName").register();
    }


}
