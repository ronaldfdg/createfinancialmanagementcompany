package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestGetSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestGetSubscriptionRequestMapper extends ConfigurableMapper implements IRestGetSubscriptionRequestMapper {

    public static final String PATH_PARAM_SUBSCRIPTION_REQUEST_ID = "subscription-request-id";

    private static final Log LOG = LogFactory.getLog(RestGetSubscriptionRequestMapper.class);

    private static final String PRODUCT_ID = "product.id";
    private static final String PRODUCT_NAME = "product.name";
    private static final String MIDDLE_NAME = "middleName";
    private static final String LAST_NAME = "lastName";
    private static final String BUSINESS_DOCUMENT_TYPE_NAME = "businessDocumentType.name";
    private static final String DOCUMENT_NUMBER = "documentNumber";
    private static final String FIRST_NAME = "firstName";
    private static final String DOCUMENT_TYPE_ID = "documentType.id";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(SubscriptionRequest.class, ModelSubscriptionRequestData.class)
                .field("id", "data.id")
                .field("business.id", "data.business.id")
                .field("business.legalName", "data.business.legalName")
                .field("business.businessManagement.managementType.id", "data.business.businessManagement.managementType.id")
                .field("business.businessManagement.managementType.description", "data.business.businessManagement.managementType.description")
                .field(PRODUCT_ID, "data.product.id")
                .field(PRODUCT_NAME, "data.product.name")
                .field("limitAmount.amount", "data.limitAmount.amount")
                .field("limitAmount.currency", "data.limitAmount.currency")
                .field("status.id", "data.status.id")
                .field("status.name", "data.status.name")
                .field("status.reason", "data.status.reason")
                .field("status.updateDate", "data.status.updateDate")
                .field("joint.id", "data.joint.id")
                .field("joint.name", "data.joint.name")
                .field("openingDate", "data.openingDate")
                .field("unitManagement", "data.unitManagement")
                .field("branch.id", "data.branch.id")
                .field("branch.name", "data.branch.name")
                .register();

        factory.classMap(BusinessDocumentSubscriptionRequest.class, ModelBusinessDocumentSubscriptionRequest.class)
                .field("businessDocumentType.id", "businessDocumentType.id")
                .field(BUSINESS_DOCUMENT_TYPE_NAME, BUSINESS_DOCUMENT_TYPE_NAME)
                .field(DOCUMENT_NUMBER, DOCUMENT_NUMBER)
                .register();

        factory.classMap(RelatedContract.class, ModelRelatedContract.class)
                .field("contract.id", "contract.id")
                .field(PRODUCT_ID, PRODUCT_ID)
                .field(PRODUCT_NAME, PRODUCT_NAME)
                .field("product.productType.id", "product.productType.id")
                .field("product.productType.name", "product.productType.name")
                .field("relationType.id", "relationType.id")
                .field("relationType.name", "relationType.name")
                .register();

        //reviewers (expand)
        factory.classMap(Reviewer.class, ModelParticipant.class)
                .field("businessAgentId", "code")
                .field(FIRST_NAME, FIRST_NAME)
                .field(MIDDLE_NAME, MIDDLE_NAME)
                .field(LAST_NAME, LAST_NAME)
                .field("secondLastName", "motherLastName")
                .field("reviewerType.id", "participantType.id")
                .field("reviewerType.name", "participantType.name")
                .register();

        factory.classMap(ContactDetail.class, ModelContactInformation.class)
                .field("contactType", "id")
                .field("contact", "contact")
                .register();

        //participands (expand)
        factory.classMap(BusinessManagerSubscriptionRequest.class, ModelBusinessManagerSubscriptionRequest.class)
                .field("id", "userId")
                .field("targetUserId", "userIdHost")
                .field(FIRST_NAME, FIRST_NAME)
                .field(MIDDLE_NAME, MIDDLE_NAME)
                .field(LAST_NAME, LAST_NAME)
                .field("secondLastName", "motherLastName")
                .register();

        factory.classMap(IdentityDocumentSubscriptionRequest.class, ModelIdentityDocumentSubscriptionRequest.class)
                .field(DOCUMENT_TYPE_ID, DOCUMENT_TYPE_ID)
                .field("documentType.name", "documentType.name")
                .field(DOCUMENT_NUMBER, DOCUMENT_NUMBER)
                .register();

    }

    @Override
    public Map<String, String> mapIn(InputGetSubscriptionRequest input) {
        LOG.info("... called method RestGetSubscriptionRequestMapper.mapIn ...");
        HashMap<String, String> map = new HashMap<>();
        map.put(PATH_PARAM_SUBSCRIPTION_REQUEST_ID, input.getSubscriptionRequestId());
        return map;
    }

    @Override
    public SubscriptionRequest mapOut(ModelSubscriptionRequestData modelResponse) {
        LOG.info("... called method RestGetSubscriptionRequestMapper.mapOut ...");
        if (modelResponse == null || modelResponse.getData() == null) {
            return null;
        }

        SubscriptionRequest response = map(modelResponse, SubscriptionRequest.class);
        if (modelResponse.getData().getBusiness() != null) {
            response.getBusiness().setBusinessDocuments(mapOutBusinessDocuments(modelResponse.getData().getBusiness().getBusinessDocuments()));

            if (response.getBusiness().getBusinessManagement() != null &&
                    response.getBusiness().getBusinessManagement().getManagementType() != null) {
                response.getBusiness().getBusinessManagement().getManagementType().setId(
                        enumMapper.getEnumValue("financialManagementCompany.businessManagement.managementType.id",
                                response.getBusiness().getBusinessManagement().getManagementType().getId()));
            }
        }

        response.setProduct(mapOutProduct(response.getProduct()));
        response.setRelatedContracts(mapOutRelatedContracts(modelResponse.getData().getRelatedContracts()));
        response.setStatus(mapOutStatus(response.getStatus()));
        response.setJoint(mapOutJoint(response.getJoint()));

        response.setReviewers(mapOutReviewers(modelResponse.getData().getParticipants()));
        response.setBusinessManagers(mapOutBusinessManagers(modelResponse.getData().getBusinessManagers()));

        return response;
    }

    private List<BusinessDocumentSubscriptionRequest> mapOutBusinessDocuments(List<ModelBusinessDocumentSubscriptionRequest> modelBusinessDocuments) {
        if (CollectionUtils.isEmpty(modelBusinessDocuments)) {
            return null;
        }
        List<BusinessDocumentSubscriptionRequest> businessDocuments = new ArrayList<>();
        for (ModelBusinessDocumentSubscriptionRequest modelBusinessDocument : modelBusinessDocuments) {
            if (modelBusinessDocument.getBusinessDocumentType() != null) {
                modelBusinessDocument.getBusinessDocumentType().setId(enumMapper.getEnumValue("subscriptionRequests.documentType.id", modelBusinessDocument.getBusinessDocumentType().getId()));
            }
            businessDocuments.add(map(modelBusinessDocument, BusinessDocumentSubscriptionRequest.class));
        }
        return businessDocuments;
    }

    private ProductSubscriptionRequest mapOutProduct(ProductSubscriptionRequest product) {
        if (product == null) {
            return null;
        }
        product.setId(enumMapper.getEnumValue("suscriptionRequest.product.id", product.getId()));
        return product;
    }

    private List<RelatedContract> mapOutRelatedContracts(List<ModelRelatedContract> modelRelatedContracts) {
        if (CollectionUtils.isEmpty(modelRelatedContracts)) {
            return null;
        }

        List<RelatedContract> relatedContracts = new ArrayList<>();
        for (ModelRelatedContract modelRelatedContract : modelRelatedContracts) {
            if (modelRelatedContract.getProduct() != null && modelRelatedContract.getProduct().getProductType() != null) {
                modelRelatedContract.getProduct().getProductType().setId(enumMapper.getEnumValue("campaigns.offers.productType", modelRelatedContract.getProduct().getProductType().getId()));
            }

            if (modelRelatedContract.getRelationType() != null) {
                modelRelatedContract.getRelationType().setId(enumMapper.getEnumValue("loans.relatedContracts.relationType", modelRelatedContract.getRelationType().getId()));
            }
            relatedContracts.add(map(modelRelatedContract, RelatedContract.class));
        }
        return relatedContracts;
    }

    private StatusSubscriptionRequest mapOutStatus(StatusSubscriptionRequest status) {
        if (status == null) {
            return null;
        }
        status.setId(enumMapper.getEnumValue("suscriptionRequest.status.id", status.getId()));
        status.setReason(enumMapper.getEnumValue("suscriptionRequest.status.reason.id", status.getReason()));
        return status;
    }

    private Joint mapOutJoint(Joint joint) {
        if (joint == null) {
            return null;
        }
        joint.setId(enumMapper.getEnumValue("joint.id", joint.getId()));
        return joint;
    }

    private List<Reviewer> mapOutReviewers(List<ModelParticipant> modelParticipants) {
        if (CollectionUtils.isEmpty(modelParticipants)) {
            return null;
        }
        List<Reviewer> reviewers = new ArrayList<>();
        for (ModelParticipant modelParticipant : modelParticipants) {
            if (modelParticipant.getParticipantType() != null) {
                modelParticipant.getParticipantType().setId(enumMapper.getEnumValue("suscriptionRequest.reviewer.id", modelParticipant.getParticipantType().getId()));
            }
            Reviewer reviewer = map(modelParticipant, Reviewer.class);
            reviewer.setContactDetails(mapOutContactDetails(modelParticipant.getContactInformations()));
            reviewers.add(reviewer);
        }
        return reviewers;
    }

    private List<ContactDetail> mapOutContactDetails(List<ModelContactInformation> modelContactInformations) {
        if (CollectionUtils.isEmpty(modelContactInformations)) {
            return null;
        }
        List<ContactDetail> contactDetails = new ArrayList<>();
        for (ModelContactInformation modelContactInformation : modelContactInformations) {
            modelContactInformation.setId(enumMapper.getEnumValue("contactDetails.contactType.id", modelContactInformation.getId()));
            contactDetails.add(map(modelContactInformation, ContactDetail.class));
        }
        return contactDetails;
    }

    private List<BusinessManagerSubscriptionRequest> mapOutBusinessManagers(List<ModelBusinessManagerSubscriptionRequest> modelBusinessManagers) {
        if (CollectionUtils.isEmpty(modelBusinessManagers)) {
            return null;
        }
        List<BusinessManagerSubscriptionRequest> businessManagers = new ArrayList<>();
        for (ModelBusinessManagerSubscriptionRequest modelBusinessManager : modelBusinessManagers) {
            BusinessManagerSubscriptionRequest businessManager = map(modelBusinessManager, BusinessManagerSubscriptionRequest.class);
            businessManager.setIdentityDocuments(mapOutIdentityDocuments(modelBusinessManager.getIdentityDocuments()));
            businessManager.setContactDetails(mapOutContactDetails(modelBusinessManager.getContactInformations()));
            businessManager.setRoles(mapOutRoles(modelBusinessManager.getRoles()));
            businessManagers.add(businessManager);
        }
        return businessManagers;
    }

    private List<IdentityDocumentSubscriptionRequest> mapOutIdentityDocuments(List<ModelIdentityDocumentSubscriptionRequest> modelBusinessDocuments) {
        if (CollectionUtils.isEmpty(modelBusinessDocuments)) {
            return null;
        }
        List<IdentityDocumentSubscriptionRequest> businessDocuments = new ArrayList<>();
        for (ModelIdentityDocumentSubscriptionRequest modelBusinessDocument : modelBusinessDocuments) {
            if (modelBusinessDocument.getDocumentType() != null) {
                modelBusinessDocument.getDocumentType().setId(enumMapper.getEnumValue("subscriptionRequests.documentType.id", modelBusinessDocument.getDocumentType().getId()));
            }
            businessDocuments.add(map(modelBusinessDocument, IdentityDocumentSubscriptionRequest.class));
        }
        return businessDocuments;
    }

    private List<Role> mapOutRoles(List<ModelRole> modeRoles) {
        if (CollectionUtils.isEmpty(modeRoles)) {
            return null;
        }
        List<Role> roles = new ArrayList<>();
        for (ModelRole modelRole : modeRoles) {
            Role role = new Role();
            role.setId(enumMapper.getEnumValue("businessManagers.role.id", modelRole.getId()));
            roles.add(role);
        }
        return roles;
    }
}
