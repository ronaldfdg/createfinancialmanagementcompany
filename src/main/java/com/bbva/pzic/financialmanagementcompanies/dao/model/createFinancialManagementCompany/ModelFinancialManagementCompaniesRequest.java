package com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany;

import java.util.List;

public class ModelFinancialManagementCompaniesRequest {

  private ModelBusiness business;
  private ModelNetcashType netcashType;
  private ModelContract contract;
  private ModelRelatedProduct relatedProduct;
  private ModelRelationType relationType;
  private List<ModelReviewerNetcash> reviewers;

  public ModelBusiness getBusiness() {
    return business;
  }

  public void setBusiness(ModelBusiness business) {
    this.business = business;
  }

  public ModelNetcashType getNetcashType() {
    return netcashType;
  }

  public void setNetcashType(ModelNetcashType netcashType) {
    this.netcashType = netcashType;
  }

  public ModelContract getContract() {
    return contract;
  }

  public void setContract(ModelContract contract) {
    this.contract = contract;
  }

  public ModelRelatedProduct getRelatedProduct() {
    return relatedProduct;
  }

  public void setRelatedProduct(ModelRelatedProduct relatedProduct) {
    this.relatedProduct = relatedProduct;
  }

  public ModelRelationType getRelationType() {
    return relationType;
  }

  public void setRelationType(ModelRelationType relationType) {
    this.relationType = relationType;
  }

  public List<ModelReviewerNetcash> getReviewers() {
    return reviewers;
  }

  public void setReviewers(List<ModelReviewerNetcash> reviewers) {
    this.reviewers = reviewers;
  }

}
