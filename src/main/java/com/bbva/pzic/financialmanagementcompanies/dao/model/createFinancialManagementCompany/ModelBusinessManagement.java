package com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany;

public class ModelBusinessManagement {

  private ModelManagementType managementType;

  public ModelManagementType getManagementType() {
    return managementType;
  }

  public void setManagementType(ModelManagementType managementType) {
    this.managementType = managementType;
  }

}
