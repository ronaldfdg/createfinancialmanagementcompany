package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany.*;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {

  private static final Log LOG = LogFactory.getLog(RestCreateFinancialManagementCompanyMapper.class);

  @Override
  public ModelFinancialManagementCompaniesRequest mapIn(DTOIntFinancialManagementCompanies financialManagementCompanies) {

    LOG.info("... called method RestCreateFinancialManagementCompanyMapper.mapIn ...");

    if(financialManagementCompanies == null) {
      return null;
    }

    ModelFinancialManagementCompaniesRequest model = new ModelFinancialManagementCompaniesRequest();
    model.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
    model.setNetcashType(mapInNetcashType(financialManagementCompanies.getNetcashType()));
    model.setContract(mapInContract(financialManagementCompanies.getContract()));
    model.setRelatedProduct(mapInRelatedProduct(financialManagementCompanies.getProduct()));
    model.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
    model.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));
    return model;

  }

  private List<ModelReviewerNetcash> mapInReviewers(final List<DTOIntReviewerNetcash> reviewersNetcash){
    if(CollectionUtils.isEmpty(reviewersNetcash)){
      return null;
    }
    return reviewersNetcash.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
  }

  private ModelReviewerNetcash mapInReviewer(final DTOIntReviewerNetcash reviewerNetcash){
    ModelReviewerNetcash modelReviewerNetcash = new ModelReviewerNetcash();

    modelReviewerNetcash.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
    modelReviewerNetcash.setContactDetails(mapInContactDetails(reviewerNetcash.getContactDetails()));
    modelReviewerNetcash.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
    modelReviewerNetcash.setUnitManagement(reviewerNetcash.getUnitManagement());
    modelReviewerNetcash.setBank(mapInBank(reviewerNetcash.getBank()));
    modelReviewerNetcash.setProfile(mapInProfile(reviewerNetcash.getProfile()));
    modelReviewerNetcash.setProfessionPosition(reviewerNetcash.getProfessionPosition());
    modelReviewerNetcash.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
    return modelReviewerNetcash;
  }

  private ModelProfile mapInProfile(final DTOIntProfile profile){
    if(profile == null) {
      return null;
    }

    ModelProfile modelProfile = new ModelProfile();
    modelProfile.setId(profile.getId());
    return modelProfile;
  }

  private ModelBank mapInBank(final DTOIntBank bank){
    if(bank == null) {
      return null;
    }

    ModelBank modelBank = new ModelBank();
    modelBank.setId(bank.getId());
    modelBank.setBranch(mapInBranch(bank.getBranch()));
    return modelBank;
  }

  private ModelBranch mapInBranch(final DTOIntBranch branch){
    if(branch == null) {
      return null;
    }

    ModelBranch modelBranch = new ModelBranch();
    modelBranch.setId(branch.getId());
    return modelBranch;
  }

  private ModelReviewerType mapInReviewerType(final DTOIntReviewerType reviewerType){
    if(reviewerType == null) {
      return null;
    }

    ModelReviewerType modelReviewerType = new ModelReviewerType();
    modelReviewerType.setId(reviewerType.getId());
    return modelReviewerType;
  }

  private List<ModelContactDetail> mapInContactDetails(final List<DTOIntContactDetail> contactDetails){
    if(CollectionUtils.isEmpty(contactDetails)){
      return null;
    }
    return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
  }

  private ModelContactDetail mapInContactDetail(final DTOIntContactDetail contactDetail){
    ModelContactDetail modelContactDetail = new ModelContactDetail();
    modelContactDetail.setContact(contactDetail.getContact());
    modelContactDetail.setContactType(contactDetail.getContactType());
    return modelContactDetail;
  }

  private ModelRelationType mapInRelationType(final DTOIntRelationType relationType){
    if(relationType == null) {
      return null;
    }

    ModelRelationType modelRelationType = new ModelRelationType();
    modelRelationType.setId(relationType.getId());
    return  modelRelationType;
  }

  private ModelRelatedProduct mapInRelatedProduct(final DTOIntRelatedProduct relatedProduct){
    if(relatedProduct == null) {
      return null;
    }

    ModelRelatedProduct modelRelatedProduct = new ModelRelatedProduct();
    modelRelatedProduct.setId(relatedProduct.getId());
    modelRelatedProduct.setProductType(mapInProductType(relatedProduct.getProductType()));
    return modelRelatedProduct;
  }

  private ModelProductType mapInProductType(final DTOIntProductType productType){
    if(productType == null) {
      return null;
    }

    ModelProductType modelProductType = new ModelProductType();
    modelProductType.setId(productType.getId());
    return modelProductType;
  }

  private ModelContract mapInContract(final DTOIntContract contract){
    if(contract == null) {
      return null;
    }

    ModelContract modelContract = new ModelContract();
    modelContract.setId(contract.getId());
    return modelContract;
  }

  private ModelNetcashType mapInNetcashType(final DTOIntNetcashType netcashType){
    if(netcashType == null) {
      return null;
    }

    ModelNetcashType modelNetcashType = new ModelNetcashType();
    modelNetcashType.setId(netcashType.getId());
    modelNetcashType.setVersion(mapInVersion(netcashType.getVersion()));
    return modelNetcashType;
  }

  private ModelVersion mapInVersion(final DTOIntVersionProduct versionProduct){
    if(versionProduct == null) {
      return null;
    }

    ModelVersion modelVersion = new ModelVersion();
    modelVersion.setId(versionProduct.getId());
    return modelVersion;
  }

  private ModelBusiness mapInBusiness(final DTOIntBusiness business){
    if(business == null) {
      return null;
    }

    ModelBusiness modelBusiness = new ModelBusiness();
    modelBusiness.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
    modelBusiness.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
    modelBusiness.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
    return modelBusiness;
  }

  private ModelLimitAmount mapInLimitAmount(final DTOIntLimitAmount limitAmount){
    if(limitAmount == null) {
      return null;
    }

    ModelLimitAmount modelLimitAmount = new ModelLimitAmount();
    modelLimitAmount.setAmount(limitAmount.getAmount());
    modelLimitAmount.setCurrency(limitAmount.getCurrency());
    return modelLimitAmount;
  }

  private ModelBusinessManagement mapInBusinessManagement(final DTOIntBusinessManagement businessManagement){
    if(businessManagement == null) {
      return null;
    }

    ModelBusinessManagement modelBusinessManagement = new ModelBusinessManagement();
    modelBusinessManagement.setManagementType(mapInManagementType(businessManagement.getManagementType()));
    return modelBusinessManagement;
  }

  private ModelManagementType mapInManagementType(final DTOIntManagementType managementType){
    if(managementType == null) {
      return null;
    }

    ModelManagementType modelManagementType = new ModelManagementType();
    modelManagementType.setId(managementType.getId());
    return modelManagementType;
  }

  private List<ModelBusinessDocument> mapInBusinessDocuments(final List<DTOIntBusinessDocument> businessDocuments){
    if(CollectionUtils.isEmpty(businessDocuments)){
      return null;
    }
    return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
  }

  private ModelBusinessDocument mapInBusinessDocument(final DTOIntBusinessDocument businessDocument){
    ModelBusinessDocument modelBusinessDocument = new ModelBusinessDocument();
    modelBusinessDocument.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
    modelBusinessDocument.setDocumentNumber(businessDocument.getDocumentNumber());
    modelBusinessDocument.setExpirationDate(businessDocument.getExpirationDate());
    modelBusinessDocument.setIssueDate(businessDocument.getIssueDate());
    return modelBusinessDocument;
  }

  private ModelBusinessDocumentType mapInBusinessDocumentType(final DTOIntBusinessDocumentType businessDocumentType){
    if(businessDocumentType == null) {
      return null;
    }

    ModelBusinessDocumentType modelBusinessDocumentType = new ModelBusinessDocumentType();
    modelBusinessDocumentType.setId(businessDocumentType.getId());
    return modelBusinessDocumentType;
  }

  @Override
  public FinancialManagementCompanies mapOut(final ModelFinancialManagementCompaniesResponse response) {
    LOG.info("... called method RestCreateFinancialManagementCompanyMapper.mapOut ...");

    if(response == null || response.getData() == null) {
      return null;
    }

    FinancialManagementCompanies financialManagementCompanies = new FinancialManagementCompanies();
    financialManagementCompanies.setId(response.getData().getId());
    return  financialManagementCompanies;

  }

}
