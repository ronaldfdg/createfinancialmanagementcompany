package com.bbva.pzic.financialmanagementcompanies.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBEHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBSHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.PeticionTransaccionKwhf;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.RespuestaTransaccionKwhf;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("txSendEmailOtpFinancialManagementCompaniesBusinessManager")
public class TxSendEmailOtpFinancialManagementCompaniesBusinessManager
        extends SingleOutputFormat<InputSendEmailOtpFinancialManagementCompaniesBusinessManager, FormatoKNECBEHF, InputSendEmailOtpFinancialManagementCompaniesBusinessManager, FormatoKNECBSHF> {

    @Resource(name = "txSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper")
    private ITxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper sendEmailOtpFinancialManagementCompaniesBusinessManagerMapper;


    public TxSendEmailOtpFinancialManagementCompaniesBusinessManager(@Qualifier("transaccionKwhf") InvocadorTransaccion<PeticionTransaccionKwhf, RespuestaTransaccionKwhf> transaction) {
        super(transaction, PeticionTransaccionKwhf::new, InputSendEmailOtpFinancialManagementCompaniesBusinessManager::new, FormatoKNECBSHF.class);
    }

    @Override
    protected FormatoKNECBEHF mapInput(InputSendEmailOtpFinancialManagementCompaniesBusinessManager inputSendEmailOtpFinancialManagementCompaniesBusinessManager) {
        return sendEmailOtpFinancialManagementCompaniesBusinessManagerMapper.mapIn(inputSendEmailOtpFinancialManagementCompaniesBusinessManager);
    }

    @Override
    protected InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapFirstOutputFormat(FormatoKNECBSHF formatoKNECBSHF, InputSendEmailOtpFinancialManagementCompaniesBusinessManager inputSendEmailOtpFinancialManagementCompaniesBusinessManager, InputSendEmailOtpFinancialManagementCompaniesBusinessManager inputSendEmailOtpFinancialManagementCompaniesBusinessManager2) {
        return sendEmailOtpFinancialManagementCompaniesBusinessManagerMapper.mapOut(formatoKNECBSHF);
    }
}
