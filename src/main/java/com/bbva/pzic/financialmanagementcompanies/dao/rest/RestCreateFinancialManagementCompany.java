package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany.ModelFinancialManagementCompaniesRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.createFinancialManagementCompany.ModelFinancialManagementCompaniesResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.bbva.pzic.financialmanagementcompanies.facade.RegistryIds.SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANY;

@Component
public class RestCreateFinancialManagementCompany extends RestPostConnection<ModelFinancialManagementCompaniesRequest, ModelFinancialManagementCompaniesResponse> {

  private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810335.backend.url";
  private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810335.backend.proxy";

  private IRestCreateFinancialManagementCompanyMapper mapper;
  private Translator translator;

  public RestCreateFinancialManagementCompany(Translator translator, IRestCreateFinancialManagementCompanyMapper mapper) {
    this.translator = translator;
    this.mapper = mapper;
  }

  @PostConstruct
  public void init(){
    useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
  }

  public FinancialManagementCompanies invoke(final DTOIntFinancialManagementCompanies input){
    return mapper.mapOut(connect(URL_PROPERTY, mapper.mapIn(input)));
  }

  @Override
  protected void evaluateResponse(ModelFinancialManagementCompaniesResponse response, int statusCode) {
    evaluateMessagesResponse(response.getMessages(), SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANY, statusCode);
  }

}
