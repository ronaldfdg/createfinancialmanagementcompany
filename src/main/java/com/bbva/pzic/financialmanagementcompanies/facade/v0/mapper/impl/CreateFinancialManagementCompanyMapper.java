package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompanyMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CreateFinancialManagementCompanyMapper implements ICreateFinancialManagementCompanyMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompanyMapper.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public DTOIntFinancialManagementCompanies mapIn(final FinancialManagementCompanies financialManagementCompanies) {
        DTOIntFinancialManagementCompanies input = new DTOIntFinancialManagementCompanies();

        input.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
        input.setNetcashType(mapInNetcashType(financialManagementCompanies.getNetcashType()));
        input.setContract(mapnInContract(financialManagementCompanies.getContract()));
        input.setProduct(mapInProduct(financialManagementCompanies.getProduct()));
        input.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
        input.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));
        return input;
    }

    private List<DTOIntReviewerNetcash> mapInReviewers(final List<ReviewerNetcash> reviewers) {
        if (CollectionUtils.isEmpty(reviewers)) {
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private DTOIntReviewerNetcash mapInReviewer(final ReviewerNetcash reviewerNetcash) {
        DTOIntReviewerNetcash dtoIntReviewerNetcash = new DTOIntReviewerNetcash();

        dtoIntReviewerNetcash.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
        dtoIntReviewerNetcash.setContactDetails(mapInContactDetails(reviewerNetcash.getContactDetails()));
        dtoIntReviewerNetcash.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
        dtoIntReviewerNetcash.setUnitManagement(reviewerNetcash.getUnitManagement());
        dtoIntReviewerNetcash.setBank(mapInBank(reviewerNetcash.getBank()));
        dtoIntReviewerNetcash.setProfile(mapInProfile(reviewerNetcash.getProfile()));
        dtoIntReviewerNetcash.setProfessionPosition(reviewerNetcash.getProfessionPosition());
        dtoIntReviewerNetcash.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());

        return dtoIntReviewerNetcash;
    }

    private DTOIntProfile mapInProfile(final Profile profile) {
        if (profile == null) {
            return null;
        }
        DTOIntProfile dtoIntProfile = new DTOIntProfile();
        dtoIntProfile.setId(profile.getId());
        return dtoIntProfile;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if (bank == null) {
            return null;
        }
        DTOIntBank dtoIntBank = new DTOIntBank();
        dtoIntBank.setId(bank.getId());
        dtoIntBank.setBranch(mapInBranch(bank.getBranch()));
        return dtoIntBank;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }
        DTOIntBranch dtoIntBranch = new DTOIntBranch();
        dtoIntBranch.setId(branch.getId());
        return dtoIntBranch;
    }

    private DTOIntReviewerType mapInReviewerType(final ReviewerType reviewerType) {
        if (reviewerType == null) {
            return null;
        }
        DTOIntReviewerType dtoIntReviewerType = new DTOIntReviewerType();
        dtoIntReviewerType.setId(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", reviewerType.getId()));
        return dtoIntReviewerType;
    }

    private List<DTOIntContactDetail> mapInContactDetails(final List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private DTOIntContactDetail mapInContactDetail(final ContactDetail contactDetail) {
        DTOIntContactDetail dtoIntContactDetail = new DTOIntContactDetail();
        dtoIntContactDetail.setContact(contactDetail.getContact());
        dtoIntContactDetail.setContactType(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", contactDetail.getContactType()));
        return dtoIntContactDetail;
    }

    private DTOIntBusiness mapInBusiness(final Business business) {
        if (business == null) {
            return null;
        }
        DTOIntBusiness dtoIntBusiness = new DTOIntBusiness();
        dtoIntBusiness.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoIntBusiness.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoIntBusiness.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        return dtoIntBusiness;
    }

    private List<DTOIntBusinessDocument> mapInBusinessDocuments(final List<BusinessDocument> businessDocuments) {
        if (CollectionUtils.isEmpty(businessDocuments)) {
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private DTOIntBusinessDocument mapInBusinessDocument(final BusinessDocument businessDocument) {
        DTOIntBusinessDocument dtoIntBusinessDocument = new DTOIntBusinessDocument();
        dtoIntBusinessDocument.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
        dtoIntBusinessDocument.setDocumentNumber(businessDocument.getDocumentNumber());
        dtoIntBusinessDocument.setIssueDate(businessDocument.getIssueDate());
        dtoIntBusinessDocument.setExpirationDate(businessDocument.getExpirationDate());

        return dtoIntBusinessDocument;
    }

    private DTOIntBusinessDocumentType mapInBusinessDocumentType(final BusinessDocumentType businessDocumentType) {
        if (businessDocumentType == null) {
            return null;
        }
        DTOIntBusinessDocumentType dtoIntBusinessDocumentType = new DTOIntBusinessDocumentType();
        dtoIntBusinessDocumentType.setId(translator.translateFrontendEnumValueStrictly("documentType.id", businessDocumentType.getId()));
        return dtoIntBusinessDocumentType;
    }

    private DTOIntLimitAmount mapInLimitAmount(final LimitAmount limitAmount) {
        if (limitAmount == null) {
            return null;
        }
        DTOIntLimitAmount dtoIntLimitAmount = new DTOIntLimitAmount();
        dtoIntLimitAmount.setAmount(limitAmount.getAmount());
        dtoIntLimitAmount.setCurrency(limitAmount.getCurrency());
        return dtoIntLimitAmount;
    }

    private DTOIntBusinessManagement mapInBusinessManagement(final BusinessManagement businessManagement) {
        if (businessManagement == null) {
            return null;
        }
        DTOIntBusinessManagement dtoIntBusinessManagement = new DTOIntBusinessManagement();
        dtoIntBusinessManagement.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoIntBusinessManagement;
    }

    private DTOIntManagementType mapInManagementType(final ManagementType managementType) {
        if (managementType == null) {
            return null;
        }
        DTOIntManagementType dtoIntManagementType = new DTOIntManagementType();
        dtoIntManagementType.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id", managementType.getId()));
        return dtoIntManagementType;
    }

    private DTOIntRelationType mapInRelationType(final RelationType relationType) {
        if (relationType == null) {
            return null;
        }
        DTOIntRelationType dtoIntRelationType = new DTOIntRelationType();
        dtoIntRelationType.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", relationType.getId()));
        return dtoIntRelationType;
    }

    private DTOIntRelatedProduct mapInProduct(final RelatedProduct product) {
        if (product == null) {
            return null;
        }
        DTOIntRelatedProduct dtoIntRelatedProduct = new DTOIntRelatedProduct();
        dtoIntRelatedProduct.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", product.getId()));
        dtoIntRelatedProduct.setProductType(mapInProductType(product.getProductType()));
        return dtoIntRelatedProduct;
    }

    private DTOIntProductType mapInProductType(final ProductType productType) {
        if (productType == null) {
            return null;
        }
        DTOIntProductType dtoIntProductType = new DTOIntProductType();
        dtoIntProductType.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", productType.getId()));
        return dtoIntProductType;
    }

    private DTOIntContract mapnInContract(final Contract contract) {
        if (contract == null) {
            return null;
        }
        DTOIntContract dtoIntContract = new DTOIntContract();
        dtoIntContract.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", contract.getId()));
        return dtoIntContract;
    }

    private DTOIntNetcashType mapInNetcashType(final NetcashType netcashType) {
        if (netcashType == null) {
            return null;
        }
        DTOIntNetcashType dtoIntNetcashType = new DTOIntNetcashType();
        dtoIntNetcashType.setId(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", netcashType.getId()));
        dtoIntNetcashType.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoIntNetcashType;
    }

    private DTOIntVersionProduct mapInVersion(final Version version) {
        if (version == null) {
            return null;
        }
        DTOIntVersionProduct dtoIntVersionProduct = new DTOIntVersionProduct();
        dtoIntVersionProduct.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", version.getId()));
        return dtoIntVersionProduct;
    }

    @Override
    public ServiceResponse<FinancialManagementCompanies> mapOut(final FinancialManagementCompanies financialManagementCompanies) {
        if (financialManagementCompanies == null) {
            return null;
        }
        return ServiceResponse.data(financialManagementCompanies).build();
    }
}
