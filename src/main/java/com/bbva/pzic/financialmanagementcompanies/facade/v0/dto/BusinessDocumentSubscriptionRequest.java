package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessDocumentSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessDocumentSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessDocumentSubscriptionRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business document type.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private BusinessDocumentType businessDocumentType;
    /**
     * Identity document number.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @DatoAuditable(omitir = true)
    private String documentNumber;

    public BusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(
            BusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
