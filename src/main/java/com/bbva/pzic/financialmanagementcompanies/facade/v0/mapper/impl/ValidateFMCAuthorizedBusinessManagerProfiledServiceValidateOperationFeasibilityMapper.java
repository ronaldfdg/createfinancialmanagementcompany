package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper
        extends ConfigurableMapper
        implements IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper {

    private static final Log LOG = LogFactory.getLog(ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper.class);

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(ValidateOperationFeasibility.class, InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class)
                .field("contract.id", "validateOperationFeasibility.contract.id")
                .field("operationAmount.amount", "validateOperationFeasibility.operationAmount.amount")
                .field("operationAmount.currency", "validateOperationFeasibility.operationAmount.currency")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility mapIn(
            final String financialManagementCompanyId,
            final String authorizedBusinessManagerId,
            final String profiledServiceId,
            final ValidateOperationFeasibility validateOperationFeasibility) {
        LOG.info("... called method ValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper.mapIn ...");
        InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input = map(validateOperationFeasibility, InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class);
        input.setFinancialManagementCompanyId(financialManagementCompanyId);
        input.setAuthorizedBusinessManagerId(authorizedBusinessManagerId);
        input.setProfiledServiceId(profiledServiceId);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public ServiceResponse<ValidateOperationFeasibility> mapOut(
            final ValidateOperationFeasibility validateOperationFeasibility) {
        LOG.info("... called method ValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper.mapOut ...");
        if (validateOperationFeasibility == null) {
            return null;
        }
        return ServiceResponse.data(validateOperationFeasibility).build();
    }
}
