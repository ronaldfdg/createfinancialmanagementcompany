package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateSubscriptionRequestMapper implements ICreateSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(CreateSubscriptionRequestMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public SubscriptionRequest mapIn(final SubscriptionRequest subscriptionRequest) {
        LOG.info("... called method CreateSubscriptionRequestMapper.mapIn ...");
        if (subscriptionRequest.getBusiness() != null) {
            subscriptionRequest.getBusiness().setBusinessDocuments(enumMapBusinessDocuments(subscriptionRequest.getBusiness().getBusinessDocuments()));
            subscriptionRequest.getBusiness().setBusinessManagement(enumMapBusinessManagement(subscriptionRequest.getBusiness().getBusinessManagement()));
        }
        subscriptionRequest.setRelatedContracts(enumMapRelatedContracts(subscriptionRequest.getRelatedContracts()));
        if (subscriptionRequest.getProduct() != null) {
            subscriptionRequest.getProduct().setId(enumMapper.getBackendValue("suscriptionRequest.product.id", subscriptionRequest.getProduct().getId()));
        }
        if (subscriptionRequest.getJoint() != null) {
            subscriptionRequest.getJoint().setId(enumMapper.getBackendValue("joint.id", subscriptionRequest.getJoint().getId()));
        }
        subscriptionRequest.setBusinessManagers(enumMapBusinessManagers(subscriptionRequest.getBusinessManagers()));
        return subscriptionRequest;
    }

    private List<BusinessDocumentSubscriptionRequest> enumMapBusinessDocuments(List<BusinessDocumentSubscriptionRequest> documents) {
        if (CollectionUtils.isEmpty(documents)) {
            return documents;
        }
        for (BusinessDocumentSubscriptionRequest dto : documents) {
            if (dto.getBusinessDocumentType() == null) {
                continue;
            }
            dto.getBusinessDocumentType().setId(
                    enumMapper.getBackendValue("subscriptionRequests.documentType.id", dto.getBusinessDocumentType().getId()));
        }
        return documents;
    }

    private BusinessManagement enumMapBusinessManagement(BusinessManagement businessManagement) {
        if (businessManagement == null) {
            return null;
        }
        if (businessManagement.getManagementType() != null) {
            businessManagement.getManagementType().setId(enumMapper.getBackendValue(
                    "financialManagementCompany.businessManagement.managementType.id", businessManagement.getManagementType().getId()));
        }
        return businessManagement;
    }

    private List<RelatedContract> enumMapRelatedContracts(List<RelatedContract> relatedContracts) {
        if (CollectionUtils.isEmpty(relatedContracts)) {
            return relatedContracts;
        }
        for (RelatedContract canonic : relatedContracts) {
            canonic.setProduct(enumMapProduct(canonic.getProduct()));
            canonic.setRelationType(enumMapRelationType(canonic.getRelationType()));
        }
        return relatedContracts;
    }

    private RelatedProduct enumMapProduct(RelatedProduct relatedProduct) {
        if (relatedProduct == null || relatedProduct.getProductType() == null) {
            return relatedProduct;
        }
        relatedProduct.getProductType().setId(
                enumMapper.getBackendValue("campaigns.offers.productType", relatedProduct.getProductType().getId()));
        return relatedProduct;
    }

    private RelationType enumMapRelationType(RelationType relationType) {
        if (relationType == null) {
            return null;
        }
        relationType.setId(
                enumMapper.getBackendValue("loans.relatedContracts.relationType", relationType.getId()));
        return relationType;
    }

    private List<BusinessManagerSubscriptionRequest> enumMapBusinessManagers(List<BusinessManagerSubscriptionRequest> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return canonics;
        }
        for (BusinessManagerSubscriptionRequest canonic : canonics) {
            canonic.setIdentityDocuments(enumMapIdentityDocuments(canonic.getIdentityDocuments()));
            canonic.setContactDetails(enumMapContactDetails(canonic.getContactDetails()));
            canonic.setRoles(enumMapRole(canonic.getRoles()));
        }
        return canonics;
    }

    private List<Role> enumMapRole(List<Role> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return canonics;
        }
        for (Role canonic : canonics) {
            canonic.setId(
                    enumMapper.getBackendValue("businessManagers.role.id", canonic.getId()));
        }
        return canonics;
    }

    private List<IdentityDocumentSubscriptionRequest> enumMapIdentityDocuments(List<IdentityDocumentSubscriptionRequest> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return canonics;
        }
        for (IdentityDocumentSubscriptionRequest canonic : canonics) {
            if (canonic.getDocumentType() == null) {
                continue;
            }
            canonic.getDocumentType().setId(
                    enumMapper.getBackendValue("subscriptionRequests.documentType.id", canonic.getDocumentType().getId()));
        }
        return canonics;
    }

    private List<ContactDetail> enumMapContactDetails(List<ContactDetail> canonics) {
        if (CollectionUtils.isEmpty(canonics)) {
            return canonics;
        }
        for (ContactDetail canonic : canonics) {
            canonic.setContactType(
                    enumMapper.getBackendValue("contactDetails.contactType.id", canonic.getContactType()));
        }
        return canonics;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<SubscriptionRequestId> mapOut(final String identifier) {
        LOG.info("... called method CreateSubscriptionRequestMapper.mapOut ...");
        SubscriptionRequestId response = new SubscriptionRequestId();
        response.setId(identifier);
        return ServiceResponse.data(response).pagination(null).build();
    }
}
