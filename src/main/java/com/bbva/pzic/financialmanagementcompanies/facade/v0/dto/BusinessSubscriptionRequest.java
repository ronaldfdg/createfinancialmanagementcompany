package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessSubscriptionRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique code that identifies the company, this code is registered in the
     * bank.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Business documents of a company.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<BusinessDocumentSubscriptionRequest> businessDocuments;
    /**
     * Information about managed business.
     */
    private BusinessManagement businessManagement;
    /**
     * Business legal name.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @DatoAuditable(omitir = true)
    private String legalName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<BusinessDocumentSubscriptionRequest> getBusinessDocuments() {
        return businessDocuments;
    }

    public void setBusinessDocuments(
            List<BusinessDocumentSubscriptionRequest> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public BusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(BusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }
}
