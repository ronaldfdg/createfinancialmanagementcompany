package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "role", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "role", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business manage role. A role is used to identify the function or
     * responsibility that a business manager performs in the company.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
