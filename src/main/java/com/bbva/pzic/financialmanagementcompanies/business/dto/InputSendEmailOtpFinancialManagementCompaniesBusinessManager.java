package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 21/09/2018.
 *
 * @author Entelgy
 */
public class InputSendEmailOtpFinancialManagementCompaniesBusinessManager {

    @Size(max = 24, groups = {ValidationGroup.SendEmailOtpFinancialManagementCompaniesBusinessManager.class,
            ValidationGroup.SendEmailOtpGetBusinessManagerData.class})
    private String businessManagerId;

    private List<DTOIntEmail> emailList;
    @NotNull(groups = ValidationGroup.SendEmailOtpFinancialManagementCompaniesBusinessManager.class)
    private String password;

    private String businessName;
    private String userName;

    public String getBusinessManagerId() {
        return businessManagerId;
    }

    public void setBusinessManagerId(String businessManagerId) {
        this.businessManagerId = businessManagerId;
    }

    public List<DTOIntEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<DTOIntEmail> emailList) {
        this.emailList = emailList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
