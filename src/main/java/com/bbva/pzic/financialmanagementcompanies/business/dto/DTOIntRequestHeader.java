package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntRequestHeader {
    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String aap;
    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String serviceID;
    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String requestID;
    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String contactID;
    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String user;

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contractID) {
        this.contactID = contractID;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
